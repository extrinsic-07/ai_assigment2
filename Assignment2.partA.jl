### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ c03d2072-cb26-11ec-3f33-73d3752169b5
using DataStructures

# ╔═╡ 2a10078b-e09d-4324-ae51-95812ab9d876
using Markdown

# ╔═╡ 8f0cf747-3fce-42ee-a8a5-b3fb8c86a82f
# MarkDown
md""" ##### Defining the Building

"""

# ╔═╡ 9a3b4424-3e3b-4c53-81ca-cc10b125b76d
mutable struct Square
	hasAgent::Bool
	floorNumber::Int
	roomNumber::Int
	NumOfparcels::Int

	struct State
	a::Array{Square}
	
end

	function Square(hasAgent, floorNumber,roomNumber, NumOfparcels )
		return new(hasAgent, floorNumber,roomNumber, NumOfparcels)
	end
end

# ╔═╡ e846297b-f230-4afc-8934-a587b47a189f
# MarkDown
md""" ##### Defining actions and cost

"""

# ╔═╡ 506768d2-3d52-4bbe-86ee-747abfb7144a
begin
	@enum Action Co Md Mu Me Mw
    Cost = Dict(Co => 5, Md => 1,Mu =>1,Me =>2,Mw =>2)
end

# ╔═╡ 73f5723f-9ffc-4f2b-87c3-7e5944165b4e
# MarkDown
md""" ##### Defining the state of the agent as well as floor, room and parcel number

"""

# ╔═╡ efdf27ba-6609-4e15-aa35-d7bc0c561100
initial_state = State([
	Square(false, 1,1,1),Square(false,1, 2,1 ),
	Square(true,2,3,2), Square(false,2,4,1),
	#Square(false,3,5,1), Square(false,3,6,1),
	#Square(false,4,7,1), Square(false,4,8,1)
	
])

# ╔═╡ a6700627-0e33-4dcf-8970-195a44b52532
struct Node
	state::State
	parent::Union{Nothing,Node}
	action::Union{Nothing,Action}
end

# ╔═╡ fea1680d-290c-4289-83e6-59ffc9f5b56a
# MarkDown
md""" ##### Transition Model

"""

# ╔═╡ f6133821-5b68-4230-8ed5-ae2e2932f627
function transition_model(node::Node, roomsPerFloor, numOfFloors)
	state = node.state
	transitions = Dict{Action,Node}()

	for (index, room) in enumerate(state.a)
		
		if(room.hasAgent)
			if(room.NumOfparcels > 0)
				tmp_state = deepcopy(state)
				tmp_state.a[index].NumOfparcels = tmp_state.a[index].NumOfparcels - 1
				transitions[Co] = Node(tmp_state,node,Co)
			end

			
			if(room.floorNumber < numOfFloors)
				tmp_state = deepcopy(state)
				tmp_state.a[index].hasAgent = false
				
				tmp_state.a[index+roomsPerFloor].hasAgent = true
				transitions[Mu] = Node(tmp_state,node,Mu)
			end
            
			if(room.floorNumber > 1)
				tmp_state = deepcopy(state)
				tmp_state.a[index].hasAgent = false
				tmp_state.a[index-roomsPerFloor].hasAgent = true
				transitions[Md] = Node(tmp_state,node,Md)
			end
           
			if(room.roomNumber < roomsPerFloor)
				tmp_state = deepcopy(state)
				tmp_state.a[index].hasAgent = false
				tmp_state.a[index+1].hasAgent = true
				transitions[Me] = Node(tmp_state,node,Me)
				
			end

			
			if(room.roomNumber > 1)
			   tmp_state = deepcopy(state)
			
				tmp_state.a[index].hasAgent = false
				tmp_state.a[index-1].hasAgent = true
				transitions[Mw] = Node(tmp_state,node,Mw)
			end
		end
	end

	return transitions

end

# ╔═╡ cd937408-ab46-49aa-9b18-f42813b32bef
 Transitions=transition_model(Node(initial_state, nothing, nothing), 2, 2)

# ╔═╡ 3bab6d32-4c48-4ba5-bc6b-69d6e3b8787d
# MarkDown
md""" ##### Goal State

"""

# ╔═╡ 3bf847df-5ae2-42ce-adb5-f25b8f4e6adf
function goal_state(state::State)
	for room in state.a
		if(room.NumOfparcels > 0)
			return false
		end
	end
	return true
end

# ╔═╡ e6199170-c721-45fa-a44a-d0e3d1de3cb9
function get_path(node::Node)
	path = [node]
	 while !isnothing(node.parent)
		 node = node.parent
		 pushfirst!(path,node)
	 end
	return path

end

# ╔═╡ 238aeaee-659d-4c94-a49a-4dec83098dd3
function solution(node::Node ,closed_list::Array)
	path = get_path(node)
    actions = []

	for node in path
		if !isnothing(node.action)
            push!(actions,node.action)
		end
	end

	cost = length(actions)
     return cost,"found ,$(length(actions)) step solution in $(length(closed_list)) steps: $(join(actions," -> "))"
end

# ╔═╡ 506081f3-e5d8-4ff0-9a02-e2e9008739e7
function failure(message::String)
   return ,message
end

# ╔═╡ 480af36f-88dd-46d3-9015-d1e3847cc71a
function calculate_total_cost(node::Node)::Int
	totalCost = 0
	path = get_path(node)
   for node in path
		if !isnothing(node.action)
            totalCost+=Cost[node.action]
		end
	end
	return totalCost
	 
end

# ╔═╡ aed989c5-6393-4f40-b4dc-2b5d7d2dc253
# MarkDown
md""" ##### Heuristic Function

"""

# ╔═╡ 07e88f68-1a7c-468c-9177-5e4a0ff37a71
function heuristic(node::Node)
	NumberOfParcels = 0
	for room in node.state.a
		NumberOfParcels += room.NumOfparcels
	end
	return NumberOfParcels * 3
end

# ╔═╡ e3a484fb-c41b-4bc5-8faf-2b448c73a033
function aStar_cost(node::Node)
   return calculate_total_cost(node) +heuristic(node)
  # return heuristic(node)
end

# ╔═╡ 2dfa4de8-efeb-475c-8647-ba49fad5a337
# MarkDown
md""" ##### Solving algorithm

"""

# ╔═╡ e07f50c2-d474-4940-9b4d-43f3baad2d28
function astarAlgorithm(start::State, numOfFloors)

	roomsPerFloor = size(initial_state.a)[1] / numOfFloors
	print(roomsPerFloor)
	
	node = Node(start,nothing,nothing)
	
	if goal_state(node.state)
        return solution(node,"Goal already reached",[])
	end
	frontier = PriorityQueue{Node,Int}()
	enqueue!(frontier,node,aStar_cost(node))
	 
	closed_list = []
	count =  0 

	while true
		if isempty(frontier)
			return failure("Could not find a solution after exhausting all options")
		end
        
		node = dequeue!(frontier)
		push!(closed_list, node.state)
		for(action,child) in transition_model(node, Int(roomsPerFloor), numOfFloors)
			count += 1
			if !(child in keys(frontier) && !(child.state in closed_list))
				if goal_state(child.state)
					return solution(child,closed_list)
				end
				
				
				enqueue!(frontier,child,aStar_cost(child))
				
			end
			
		end
        if (count>10000)
            return failure("timed out")
		end
	end
       
end

# ╔═╡ d412d16b-3efe-4797-849c-5ee663cc1100
astarAlgorithm(initial_state, 2)

# ╔═╡ a7f485be-925c-484f-b251-8d4f2babdf72


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"

[compat]
DataStructures = "~0.18.11"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═c03d2072-cb26-11ec-3f33-73d3752169b5
# ╠═2a10078b-e09d-4324-ae51-95812ab9d876
# ╟─8f0cf747-3fce-42ee-a8a5-b3fb8c86a82f
# ╠═9a3b4424-3e3b-4c53-81ca-cc10b125b76d
# ╟─e846297b-f230-4afc-8934-a587b47a189f
# ╠═506768d2-3d52-4bbe-86ee-747abfb7144a
# ╟─73f5723f-9ffc-4f2b-87c3-7e5944165b4e
# ╠═efdf27ba-6609-4e15-aa35-d7bc0c561100
# ╠═a6700627-0e33-4dcf-8970-195a44b52532
# ╟─fea1680d-290c-4289-83e6-59ffc9f5b56a
# ╠═f6133821-5b68-4230-8ed5-ae2e2932f627
# ╟─cd937408-ab46-49aa-9b18-f42813b32bef
# ╟─3bab6d32-4c48-4ba5-bc6b-69d6e3b8787d
# ╠═3bf847df-5ae2-42ce-adb5-f25b8f4e6adf
# ╠═e6199170-c721-45fa-a44a-d0e3d1de3cb9
# ╠═238aeaee-659d-4c94-a49a-4dec83098dd3
# ╠═506081f3-e5d8-4ff0-9a02-e2e9008739e7
# ╠═480af36f-88dd-46d3-9015-d1e3847cc71a
# ╟─aed989c5-6393-4f40-b4dc-2b5d7d2dc253
# ╠═07e88f68-1a7c-468c-9177-5e4a0ff37a71
# ╠═e3a484fb-c41b-4bc5-8faf-2b448c73a033
# ╟─2dfa4de8-efeb-475c-8647-ba49fad5a337
# ╠═e07f50c2-d474-4940-9b4d-43f3baad2d28
# ╠═d412d16b-3efe-4797-849c-5ee663cc1100
# ╠═a7f485be-925c-484f-b251-8d4f2babdf72
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
